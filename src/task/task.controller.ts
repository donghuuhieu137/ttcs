import { Body, Controller, Get, Post } from '@nestjs/common';
import { TaskService } from './task.service';

@Controller('tasks')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}
  @Get()
  async get() {
    const tasks = await this.taskService.get();
    return tasks;
  }

  @Post()
  async post(@Body() dto: any) {
    const task = await this.taskService.post(dto);
    return task;
  }
}
