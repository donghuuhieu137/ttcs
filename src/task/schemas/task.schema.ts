import { plainToInstance } from 'class-transformer';
import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { TransformObjectId } from '../../common/decorators/tranform-object-id';

export type TaskDocument = Task & Document;

@Schema({ timestamps: true, versionKey: false })
export class Task {
  @TransformObjectId()
  _id: string;

  @Prop({ length: 100, required: true, type: String })
  name: string;

  @Prop({ type: Number })
  amount: number;

  @Prop({ length: 225, type: String })
  status: string;

  constructor(partial: Partial<Task>) {
    Object.assign(this, partial);
  }
}

export const TaskSchema = SchemaFactory.createForClass(Task);

TaskSchema.methods.toJSON = function () {
  return plainToInstance(Task, this.toObject());
};
