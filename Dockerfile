FROM node:14.17-alpine

COPY ["package.json", "nest-cli.json", "tsconfig.build.json", "tsconfig.json", "/app/"]

WORKDIR /app

RUN ls -al
RUN npm install

COPY ["src", "/app/src"]

RUN ls -al
RUN npm run build

CMD ["npm", "run", "start:prod"]
